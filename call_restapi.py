
import requests 
import cv2 
import time 

# video_red = cv2.VideoCapture("/media/thanhlnbka/data/thanhlnbka/Projects/Test/videos/34M29S_1630715669.mp4")
video_red = cv2.VideoCapture("videos/video1.mp4")
count = 0
time_ = []
while True:
    t1 = time.time()
    _, frame = video_red.read()
    if frame is None:
        continue
    frame = cv2.resize(frame, (640,480))
    multiple_files = [("cam1", cv2.imencode(".jpg", frame)[1].tobytes())]
    # cv2.imwrite("some_img.jpg", frame)
    # multiple_files = [("cam1",open("some_img.jpg", "rb"))]
    
    res = requests.post("http://10.10.2.111:1998/predictions/persondet/0.1", files=multiple_files)
    # print(res.json())
    
    data_json = res.json()
    try:
        for d in data_json[0]:
            boxes = [int(i) for i in d["boxes"]]
            label = d["class"]
            score = d["score"]
            if float(score) >= 0.5:
                cv2.rectangle(frame, (boxes[0], boxes[1]), (boxes[2], boxes[3]), (255,22,20), thickness=2)
                cv2.putText(frame, label, (boxes[0],boxes[1]-10), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,22,20), 2, cv2.LINE_AA)
    except: 
        print("huhu")

   
    cv2.imshow('Frame RESTAPI',frame)
    print("call api time: {} s".format(time.time()-t1))
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    time_.append(time.time()-t1)
    count += 1
    # if count == 200:
    #     print("time avg 200 frame first :{}".format(sum(time_)/len(time_)))
    #     break

    
video_red.release()
cv2.destroyAllWindows()
