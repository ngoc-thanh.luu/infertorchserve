import sys 
sys.path.append("outs_grpc")
import grpc
import inference_pb2
import inference_pb2_grpc
import management_pb2
import management_pb2_grpc
import sys
import cv2 
import time 
from ast import literal_eval
from glob import glob 
import os, sys 
import cv2
from tqdm import tqdm 

def get_inference_stub():
    channel = grpc.insecure_channel('10.10.2.111:7000')
    stub = inference_pb2_grpc.InferenceAPIsServiceStub(channel)
    return stub


def get_management_stub():
    channel = grpc.insecure_channel('10.10.2.111:7001')
    stub = management_pb2_grpc.ManagementAPIsServiceStub(channel)
    return stub


def infer(stub, model_name):
    video_read = cv2.VideoCapture("videos/video1.mp4")
    count = 0
    time_ = []
    while True:
        t1 = time.time()
        _, frame = video_read.read()
        if frame is not None:
            frame = cv2.resize(frame, (640,480))
            input_data = {'cam1': cv2.imencode(".jpg", frame)[1].tobytes()}
            
            response = stub.Predictions(
                inference_pb2.PredictionsRequest(model_name=model_name,
                                                input=input_data))

            try:
                prediction = response.prediction.decode('utf-8')
                data_json = literal_eval(prediction)
                for d in data_json[0]:
                    boxes = [int(i) for i in d["boxes"]]
                    label = d["class"]
                    conf = round(float(d["score"]), 2)
                    if conf > 0.35:
                        cv2.rectangle(frame, (boxes[0], boxes[1]), (boxes[2], boxes[3]), (255,22,20), thickness=2)
                        cv2.putText(frame, f"{label}:{conf}", (boxes[0],boxes[1]-10), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,22,20), 1, cv2.LINE_AA)

                
            except grpc.RpcError as e:
                exit(1)
            
        cv2.imshow("FRAME GRPC API", frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        print("time predict: {}s".format(time.time()-t1))
        time_.append(time.time()-t1)
        count += 1
        # if count == 200:
        #     print("time avg 200 frame first :{}".format(sum(time_)/len(time_)))
        #     break


def infer_folder(stub, model_name, pth_fls):
    if not os.path.isdir("out_video_infer"):
        os.mkdir("out_video_infer")
    namefolders = os.listdir(pth_fls)
    for namefolder in namefolders:
        pth_folder = os.path.join(pth_fls, namefolder)
        files_ = glob(os.path.join(pth_folder,"img1", "*.jpg"))
        files = sorted(files_, key=lambda x: int(x.split("/")[-1][:-4]))
        count = 0
        time_ = []
        out = cv2.VideoWriter(f'out_video_infer/{namefolder}.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 10, (640,480))
        for file in files:
            t1 = time.time()
            frame = cv2.imread(file)
            if frame is not None:
                frame = cv2.resize(frame, (640,480))
                input_data = {'cam1': cv2.imencode(".jpg", frame)[1].tobytes()}
                
                response = stub.Predictions(
                    inference_pb2.PredictionsRequest(model_name=model_name,
                                                    input=input_data))

                try:
                    prediction = response.prediction.decode('utf-8')
                    data_json = literal_eval(prediction)
                    for d in data_json[0]:
                        boxes = [int(i) for i in d["boxes"]]
                        label = d["class"]
                        conf = round(float(d["score"]), 2)
                        if conf > 0.35:
                            cv2.rectangle(frame, (boxes[0], boxes[1]), (boxes[2], boxes[3]), (255,22,20), thickness=1)
                            cv2.putText(frame, f"{conf}", (boxes[0],boxes[1]-10), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (255,22,20), 1, cv2.LINE_AA)
                    
                except grpc.RpcError as e:
                    exit(1)
                
            # time.sleep(1)
            cv2.imshow("FRAME GRPC API", frame)
            out.write(frame)
            
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
            print("time predict: {}s".format(time.time()-t1))
            time_.append(time.time()-t1)
            count += 1
        out.release()
        cv2.destroyAllWindows()



if __name__ == '__main__':
     infer(get_inference_stub(),"persondet")
    #  infer_folder(get_inference_stub(), "persondet", "/media/thanhlnbka/data/thanhlnbka/Data/MOT20/test")